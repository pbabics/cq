# unnecessary else after return
def elif_then_return() -> None:
	' C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), C0301(line-too-long), '
	if True:  # using-constant-test, should produce an error
		return
	else:
		return  # no-else-return, warning, not shown unless --show-warnings is on
